import { LatLngExpression } from "leaflet";

const cities = [
     {
      "id": 1,
      "name": "Paris",
      "coordinates":[48.8589507, 2.2770199], 
      "important": true
    },
    {
      "id": 2,
      "Name": "Budapest", 
      "coordinates":[47.4813602, 18.9902187], 
      "important": true
    },
{
      "id": 3,
      "Name": "Bern", 
      "coordinates":[46.9545845,7.2547869], 
      "important": true
    },
{
      "id": 4,
      "Name": "Milan", 
      "coordinates":[45.4627124,9.1076924], 
      "important": true
    },
{
      "id": 5,
      "Name": "New York", 
      "coordinates":[40.6971494,-74.2598672], 
      "important": true
    },
{
      "id": 6,
      "Name": "Abu Dhabi", 
      "coordinates":[24.3865729,54.2784263], 
      "important": true
    },
{
      "id": 7,
      "Name": "Los Angeles", 
      "coordinates":[34.0201613,-118.6919224], 
      "important": true
    },
{
      "id": 8,
      "Name": "Rio De Janeiro", 
      "coordinates":[-22.913885,-43.7261767], 
      "important": true
    },
{
      "id": 9,
      "Name": "Tel Aviv", 
      "coordinates":[32.0879122,34.7272057], 
      "important": true
    },


]

export default cities
