import React from 'react'
const posts = [
     {
      "id": 1,
      "date" : "10/10/2020",
      "title": "Finally online, Here we are!",
      // Remember to create a single element with <> </> or react fragments
      "content":<> <p>Finally, after different months working on my personal website, I am online. The effort has been huge but I am happy to present this new project. I started coding during the first quarantine and immediately I thought: What can make it cool?<i className="fas fa-lg fa-globe"></i>. I started drafting and going through <a href="https://stackoverflow.com/"> StackOverflow </a> to look at possible designs, and here it is my fully coded website. I will use this space to update you about my upcoming project and I will use the hashtag to group the different interests (they are a lot). Clicking on hashtag, will appear only the posts related with it. </p></>,
      "hash": ['website', 'Reactjs']
    },
    {
      "id": 2,
      "title": "Graphs and C++ : where to find a good implementation?",
      "date" : "29/10/2023",
      // Remember to create a single element with <> </> or react fragments
      "content":<> <p> In the last months I have not being so present due to some work constraint. In particular I got interested to the 
        <a href="https://en.wikipedia.org/wiki/Graph_theory"> Graph Theory</a>. Actually at the beginning I was not so into this kind of topic and hence I had to do a 
        huge bibliographic research and I discovered an whole world of techniques to store and explore graphs.
         First of all to make you sure that you understand that graph are everywhere 
         (unluckly we are in a world where there is the axiome "if I do not see I do not believe", ask to St.Thomas for clarifications),
        watch <a href="https://www.youtube.com/watch?v=9c2HAwnVA9k&t=631s&ab_channel=CDEChannel"> this video</a>.  Graphs are everywhere: in CFD, computer science, finance, 
        hi-tech, biology etc. So I thought it was worth it to get familiar with their representaions and algorithms not only for my 
        professional scope but for my personal culture. So I explored different ways to store graphs 
        (starting from the <a href="https://en.wikipedia.org/wiki/Adjacency_matrix"> Adjacency Matrix</a>, to the <a href="https://en.wikipedia.org/wiki/Linked_list#:~:text=In%20computer%20science%2C%20a%20linked,which%20together%20represent%20a%20sequence."> Linked list</a>) and different ways to explore the graphs ). 
        To understand better graphs algorithms and different ways to explore them
        I have tried to find a direct application and hence I published an open-source library (<a href="https://github.com/albiremo/dualGPy"> DualGPy</a>) to apply the concepts
        to a mesh (a mesh is nothing more than an indirect graph).
        </p>
         </>,
      "hash": ['graphs']
    },
]

export default posts
