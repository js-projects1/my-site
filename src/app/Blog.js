import React, { useState, useEffect,useCallback }  from 'react'
import posts from '../data/Posts.js'

class Blog extends React.Component {
// I set the state with the open property to define later the push of an hastag and the isActive property for
// the filtering
state= {
	open: {},
	isActive : false,
}
// The idea is to assign all the hashtags and later to update the state in the callback and to use the filter method over the posts
// I define the handleClick
handleClick = (k) => {
   // Define the openState when I click
   let linkOpenState = true;
   // Check if I have already clicked
   if (this.state.open.hasOwnProperty(k)) {
   // If I have already clicked do the contrary of before, by negating the linkOpenState assigned previously
        linkOpenState = !this.state.open[k];
   }
   // Assign the linkOpenState
    this.setState({ open: { [k]: linkOpenState } ,
                    isActive: !this.state.isActive,
    })
// Debug check
console.log(k)
console.log(linkOpenState)
console.log(this.state.open[k]);
}

render(){
// Constitute the filtered vector with respect to the isActive attribute
// Object.keys(this.state.open) returns the properties of the state
const filtered = this.state.isActive?  posts.filter(k=>k.hash.find(j=>j==Object.keys(this.state.open))) : posts
return(
filtered.map((i,index)=>(
// To center the Bulma box we have to open first a columns and later a column
<div class="columns is-mobile is-centered">
<div className="column is-half">
  <div class="box" key={index}>
    <article class="media">
      <div class="media-content">
        <div class="content">
        <h1 class="title">{i.title}</h1>
        <p class="is-italic">{i.date}</p>
            {i.content}
        </div>
        <div class="tags">
         {i.hash.map(k=>(<span key={k} id={k} onClick={() => this.handleClick(k)}  class={this.state.open[k]? "tag is-primary" : "tag"} >{k}</span>))}
        </div>
      </div>
    </article>
  </div>
 </div>
</div>
))
)}}
export default Blog
