import React from 'react'
import { NavLink} from 'react-router-dom'
const Overview = () => (
  <section class="hero">
        <div class="container is-max-desktop">
    <div class="hero-body">
      <p class="title has-text-centered">
        Overview of the projects
      </p>
      </div>
<hr className="navbar-divider" />
<div class="tile is-ancestor">
  <div class="tile is-vertical is-16">
    <div class="tile">
      <div class="tile is-parent is-vertical">
        <NavLink className="tile is-child notification is-warning" to="/my-site/master">
          <p class="title">Generalized Kolmogorov Equation for Wall-Bounded Flows with Turbulent Drag Reduction</p>
          <p class="subtitle">Developement of an efficient framework for Generalized Kolmogorov Equation (GKE) analysis</p>
          </NavLink>
          <NavLink className="tile is-child notification is-danger" to="/my-site/site">
          <p class="title">This site</p>
          <p class="subtitle">Best site in the world</p>
        </NavLink>
        <NavLink  class="tile is-child notification is-info" to="/my-site/phd">
          <p class="title">Numerical modeling of an aeronautical injector: from the internal flow to the dispersed spray</p>
          <p class="subtitle">Developement of a multiphase model and of an innovative phase analysis</p>
        </NavLink>
      </div>
      <div class="tile is-parent is-vertical">
        <NavLink  class="tile is-child notification is-warning" to="/my-site/graph">
          <p class="title">Graph theory</p>
          <p class="subtitle">Agglomeration for non-structured meshes.</p>
        </NavLink>
        <NavLink  class="tile is-child notification is-danger" to="/my-site/remeshing">
          <p class="title">Remeshing technology for turbomachinery applications</p>
          <p class="subtitle">Developement of numerical methods to apply remeshing to turbomachinery applications.</p>
        </NavLink>
      </div>
    </div>
  {/*  <div class="tile is-parent">
      <article class="tile is-child notification is-danger">
        <p class="title">Wide tile</p>
        <p class="subtitle">Aligned with the right tile</p>
        <div class="content">
        </div>
      </article>
    </div>
    */}
  </div>
</div>
</div>
  </section>
)
export default Overview
