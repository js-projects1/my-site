import React from 'react'
const Site = () => (
<section class="hero">
<div class="container is-max-desktop">
	<div class="hero-body has-text-centered">
		<p class="title">
		  This site
		</p>
		<p class="subtitle">
  		an interesting project
		</p>
<div class="tile is-ancestor">
  <div class="tile is-vertical is-16">
    <div class="tile">
      <div class="tile is-parent is-vertical">
        <div className="tile is-child notification is-warning">
          <p class="title">Enterily written in React</p>
          <p class="subtitle">Thought in a 'reactive' way </p>
          </div>
          <div className="tile is-child notification is-danger">
          <p class="title">Open Source</p>
          <p class="subtitle">Code available on GitLab to inspire people</p>
        </div>
      </div>
      <div class="tile is-parent">
        <div  class="tile is-child notification is-info">
          <p class="title">Use of Bulma</p>
          <p class="subtitle">Use of an innovative CSS framework</p>
        </div>
      </div>
    </div>
</div>
</div>
</div>
It was during the first lockdown that I started to design this website. First of all I decided to learn coding in JavaScript, following the unbelievably well formatted course <a href="https://fullstackopen.com/en/#course-contents"><strong> Full Stack Open  </strong></a>. Day by day I got in the mechanism of functional programming  (ask to <a href="https://www.youtube.com/c/funfunfunction"><strong> fun fun functions</strong></a> to understand what does it mean) and of <a href="https://reactjs.org/"><strong> React  </strong></a> framework (always remember to think is a reactive way). I started developing my own ideas and discover the magic behind it.
</div>
</section>
)
export default Site
