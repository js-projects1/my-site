import React from 'react'
const Master = () => (
  <section class="hero">
        <div class="container is-max-desktop">
    <div class="hero-body has-text-centered">
      <p class="title">
        Generalized Kolmogorov Equation for Wall-Bounded Flows with Turbulent Drag Reduction
      </p>
      <p class="subtitle">
        Developement of an efficient framework for Generalized Kolmogorov Equation (GKE) analysis
      </p>
      </div>
The present work studies how the energetic properties of turbulent flows confined between two plane parallel walls are modified by turbulent skin-friction drag reduction strategies.
A new Direct Numerical Simulation (DNS) database of turbulent channels at low Reynolds numbers - canonical or modified by skin-friction drag reduction strategies - is produced to provide high-fidelity turbulent flow data for the present analysis. The peculiar feature of this new set of DNS simulations is the strategy adopted to drive the flow throughout the channel via the Constant Power Input (CPI) approach, according to which the power provided to the flow via pumping is kept constant. CPI is the enabling strategy, which allows the direct comparison of energy transfer rates among canonical and drag-reduced channels.
The single-point Turbulent Kinetic Energy (TKE) and Mean Kinetic Energy (MKE) budget equations are analysed in order to achieve a detailed description of the control-induced modifications on energy transfer rates. The integral TKE and MKE budget equations, represented compactly in terms of a energy box, are analysed in the search for general trends, i.e. common to different control strategies, regarding the effects of drag reduction upon the way in which turbulent flow transforms and dissipates the power which is provided to it. The more the control strategy is effective (i.e the more it increases the flow rate) the more the peak of production of TKE is weakened. The energy box representation has shown that it is still hard to draw a conclusion about the dissipative behaviour of the control techniques.
The budget equation for the second-order structure function in turbulent channels, the so-called Generalized Kolmogorov Equation (GKE), is thus exploited to inspect the energy fluxes occurring simultaneously in the physical space and among different scales. The goal is to study whether and how turbulent drag-reduction strategies modify such fluxes, which represent statistical description of the energy cascade. A new tool that solves the GKE has been implemented and it turned out to be more than two order of magnitudes faster than the previous implementations. The analysis of the outcomes confirmed and expanded the results of the TKE and MKE budgets, highlighting differences in the energy paths between canonical and drag-reduced channels.
</div>
{/* start spacing*/}
  <div class="columns">
      <div class="column is-12">
      </div>
  </div>
{/* end spacing */}
<div class="block has-text-centered">
<button class="button is-rounded">
  <span class="icon is-small">
    <i class="fa fa-download"></i>
  </span>
  <a href="https://home.aero.polimi.it/quadrio/it/Tesi/remigi/tesi-remigi.pdf">Thesis</a>
</button>
<button class="button is-rounded">
  <span class="icon is-small">
    <i class="fa fa-download"></i>
  </span>
       <a href="https://www.researchgate.net/profile/Maurizio-Quadrio/publication/335763159_An_efficient_numerical_method_for_the_generalised_Kolmogorov_equation/links/5d963f1a299bf1c363f584a9/An-efficient-numerical-method-for-the-generalised-Kolmogorov-equation.pdf?_sg%5B0%5D=Stm4ZXzaz5IysSYf_TCD4EfyBiy05-QAT9ylqGYFjA5V5mctIroG5bD2qBvHiLXsI4CjbaN9abiLpbQ8Q9kA4w.8UEvZuZtXsmyCUEqITA5aSuQeZthSLy63-_VmXh0dlPf-2-x6rUZ7SZ98TGcNL2WbN-1CiJutegJV4bmKtWVgA&_sg%5B1%5D=7I0eVKBCRcd4ZOdYyZJ0C7x41n2LWWInII8bQE6oDKETKpvtj9XmqnnCeg9h8YrCLmbfwcFkdcJi_eOY4TRbShZFvgEg4J7Sn69I-f3-1Qg3.8UEvZuZtXsmyCUEqITA5aSuQeZthSLy63-_VmXh0dlPf-2-x6rUZ7SZ98TGcNL2WbN-1CiJutegJV4bmKtWVgA&_iepl=">Article</a>
</button>

</div>
  </section>


)
export default Master
