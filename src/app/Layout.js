import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Site from './layout/Site'
import Header from './layout/Header'
import Content from './layout/Content'
import Footer from './layout/Footer'
import Router from './layout/Router'

const Layout = ({ children }) => (
  <Site>
    <Helmet
      title="Alberto.Remigi"
      meta={[
        { name: 'description', content: 'Alberto\'s personal website, portfolio, blog, tutorials, and just cool $h!t' },
        { name: 'keywords', content: 'resume, blog, porfolio, tutorials, albiremo, coding, Alberto' },
      ]}
      script={[
        { 'src': 'https://use.fontawesome.com/releases/v5.3.1/js/all.js'},
	{ 'src': 'https://unpkg.com/leaflet@1.2.0/dist/leaflet.js'},
      ]}
    />
    <Header/>
    <Content>
       <Router/>
    </Content>
    <Footer/>
  </Site>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout
