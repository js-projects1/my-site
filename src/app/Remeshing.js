import React from 'react'
const Remeshing = () => (
  <section class="hero">
        <div class="container is-max-desktop">
    <div class="hero-body has-text-centered">
      <p class="title">
      Remeshing technology for turbomachinery applications
      </p>
      <p class="subtitle">
        Building it
      </p>
      <img src={require('./images/tatef.png')} class="center"/>
      </div>  
 </div>
{/* start spacing*/}
  </section>
)
export default Remeshing