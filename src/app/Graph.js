import React from 'react'
const Graph = () => (
  <section class="hero">
        <div class="container is-max-desktop">
    <div class="hero-body has-text-centered">
      <p class="title">
      Graph theory applied to agglomeration of non-structured meshes
      </p>
      <p class="subtitle">
        Building it
      </p>
      <img src={require('./images/profile.png')} class="center"/>
      </div>  
 </div>
{/* start spacing*/}
  </section>
)
export default Graph