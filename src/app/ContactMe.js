import React, { useState, useEffect,useCallback }  from 'react';
import emailjs from "emailjs-com";
import Swal from 'sweetalert2'
const { REACT_APP_USER_MAIL_ID } = process.env;
class ContactMe extends React.Component {
	state = { feedabck: '',
                      name: '',
                      email: '' 
                     };
      // saves the user's name entered to state
      nameChange = (event) => {
        this.setState({name: event.target.value})
      }
      
      // saves the user's email entered to state
      emailChange = (event) => {
        this.setState({email: event.target.value})
      }

      // saves the user's message entered to state
      messageChange = (event) => {
        this.setState({feedback: event.target.value})
      }

      //onSubmit of email form
      handleSubmit = (event) => {
        event.preventDefault();

        //This templateId is created in EmailJS.com
        const templateId = 'Default';
    
        //This is a custom method from EmailJS that takes the information 
        //from the form and sends the email with the information gathered 
        //and formats the email based on the templateID provided.
        this.sendFeedback(templateId, {
                                        message: this.state.feedback, 
                                        name: this.state.name, 
                                        email: this.state.email
                                       }
                         )

      }
    
      //Custom EmailJS method
      sendFeedback = (templateId, variables) => {
	const userid ={ REACT_APP_USER_MAIL_ID };
        emailjs.send(
          'basic', templateId,
          variables,userid.REACT_APP_USER_MAIL_ID).then(res => {
            // Email successfully sent alert
            Swal.fire({
              title: 'Email Successfully Sent',
              icon: 'success'
            })
          })
          // Email Failed to send Error alert
          .catch(err => {
            Swal.fire({
              title: 'Email Failed to Send',
              icon: 'error'
            })
            console.error('Email Error:', err)
          })
      }
render(){
return(
<>
<div>
<div class="field">
  <label class="label">Name</label>
  <div class="control">
    <input class="input" type="text" placeholder="Text input"id="name" onChange={this.nameChange} required/>
  </div>
</div>
</div>
<div>
<div class="field">
  <label class="label">Email</label>
  <div class="control has-icons-left has-icons-right">
    <input class="input" type="email" id="email" onChange={this.emailChange} required/>
    <span class="icon is-small is-left">
      <i class="fas fa-envelope"></i>
    </span>
  </div>
</div>
</div>
<div>
<div class="field">
  <label class="label">Message</label>
  <div class="control">
    <textarea class="textarea"i id="message"
                  name="message"
                  onChange={this.messageChange} placeholder="Textarea"></textarea>
  </div>
</div>
<div>
<div class="field is-grouped">
  <div class="control">
    <button class="button is-link" onClick={this.handleSubmit}>Submit</button>
</div>
</div>
</div>
</div>
</>
)}}
export default ContactMe
