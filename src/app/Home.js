import React from 'react'
import { StyleRoot } from 'radium'
import Button from './layout/Button'
import { Link} from 'react-router-dom'
import Timeline from './layout/timeline/timeline'
import ProgressBar from './layout/ProgressBar'
import VisibilitySensorImage from './layout/effects/VisibilitySensorImage'
// Test env variable
const { REACT_APP_MY_ENV } = process.env;
console.log(REACT_APP_MY_ENV);
// end test
const Home = () => {
return(
<div>
<div className="box">
<section className="hero">
  <div className="hero-body has-text-centered">
    <div className="container is-fullhd">
      <h1 className="title">
        Alberto Remigi
      </h1>
      <h2 className="subtitle">
        Passionate CFD guy and Graphs NERD
      </h2>
   </div>
  </div>
</section>
</div>
<StyleRoot>
	<Timeline activeColor='#d7dbde'>
		<div icon=<i className="fas fa-birthday-cake"></i>>
			<div className="box">
				<div className="container is-max-desktop">
      					<h1 className="title">
        					1992
      					</h1>
   				</div>
    			</div>
			<VisibilitySensorImage value={"10"}/>
  			{/* start spacing*/}
  			<div class="columns">
      				<div class="column is-12"></div>
  			</div>
  			{/* end spacing */}
			<h1 className="subtitle">	Alberto Remigi was born in <a href="https://goo.gl/maps/32ioYYJfR6jPqwSu9"><strong>Monza</strong></a> (Italy) </h1>
			<div>
				The 24th of October 1992 I was born in Monza. Since the beginning I developed a strong passion
				for Science   <i className='fas fa-microscope'></i>, Cars   <i className='fas fa-car'></i>, and Music   <i className='fas fa-music'></i>.
			</div>
		  	{/* start spacing*/}
 			<div class="columns">
      				<div class="column is-12"></div>
  			</div>
 			<div class="columns">
      				<div class="column is-12"></div>
	        	</div>
               		{/* end spacing */}
				<img src={require('./images/small.jpg')}/>
		</div>
	        {/*Second section: The master thesis*/}
		<div>
			<div className="box">
				<div className="container is-max-desktop">
      					<h1 className="title">
        					2017
      					</h1>
   				</div>
   			</div>
    			<VisibilitySensorImage value={"20"}/>
    			<h1 className="subtitle">	Master Degree in Aerodynamics at  <a href="https://www.polimi.it/?lang=IT"><strong>Politecnico di Milano</strong></a> </h1>
      				I get Master Degree in Aerodynamics <i className='fas fa-plane'></i> at Politecnico di Milano. Big dreams in my mind. I carry out my master thesis in  <a href="https://www.karlsruhe.de/"><strong> Karlsruhe </strong></a> at <a href="https://www.kit.edu/"><strong> KIT  </strong></a>.
      			{/* start spacing*/}
        		<div class="columns">
            			<div class="column is-12"></div>
        		</div>
      			{/* end spacing */}
      			<Link to='/my-site/master'>
    				<button class="button is-link">
          				About
    				</button>
			</Link>
  			{/* start spacing*/}
  			<div class="columns">
      				<div class="column is-12"></div>
  			</div>
  			<div class="columns">
      				<div class="column is-12"></div>
  			</div>
  			{/* end spacing */}
			<img src={require('./images/laurea.jpg')}/>
	     </div>
	     {/*Third  section: The PhD*/}
	     <div>
			<div className="box">
				<div className="container is-max-desktop">
      					<h1 className="title">
        					2021
      					</h1>
   				</div>
   			</div>
    			<VisibilitySensorImage value={"30"}/>
    			<h1 className="subtitle">	PhD in Physics and Fluid Mechanics </h1>
      After a brief experience in Germany, I settle in the <a href="https://www.paris.fr/"><strong> city of lights </strong></a>  and
      I get PhD in Physics and Fluid Mechanics
      			{/* start spacing*/}
        		<div class="columns">
            			<div class="column is-12"></div>
        		</div>
      			{/* end spacing */}
      			<Link to='/my-site/PhD'>
    				<button class="button is-link">
          				About
    				</button>
			</Link>
  			{/* start spacing*/}
  			<div class="columns">
      				<div class="column is-12"></div>
  			</div>
  			<div class="columns">
      				<div class="column is-12"></div>
			</div>
  			{/* end spacing */}
			<img src={require('./images/dottorato.jpg')}/>

	  </div>
	     {/*Fourth  section: ONERA*/}
	  <div>
			<div className="box">
				<div className="container is-max-desktop">
      					<h1 className="title">
        					2021
      					</h1>
   				</div>
   			</div>
    			<VisibilitySensorImage value={"40"}/>
    			<h1 className="subtitle"> Researcher in Numerical Methods and Software Engineer </h1>
				I develop Graph Algorithms for unstructured meshes at <a href="https://www.onera.fr/en"><strong> ONERA </strong></a>.
	                {/* start spacing*/}
        		<div class="columns">
            			<div class="column is-12"></div>
        		</div>
      			{/* end spacing */}
      			<Link to='/my-site/graph'>
    				<button class="button is-link">
          				About
    				</button>
			</Link>
			{/* start spacing*/}
						  <div class="columns">
      				<div class="column is-12"></div>
  			</div>
  			<div class="columns">
      				<div class="column is-12"></div>
			</div>
  			{/* end spacing */}
			<img src={require('./images/profile.png')} class="center"/>
  
	  </div>
	     {/*Fifth  section: SAFRANTECH*/}
		 <div>
			<div className="box">
				<div className="container is-max-desktop">
      					<h1 className="title">
        					2022
      					</h1>
   				</div>
   			</div>
    			<VisibilitySensorImage value={"60"}/>
    			<h1 className="subtitle"> Researcher in Numerical Methods and Software Engineer </h1>
				I develop strategies and methods to apply remeshing technology to turbomachinery applications at <a href="https://www.safran-group.com/group/innovation/safran-tech"><strong> SAFRANTECH </strong></a>
				in collaboration with  <a href="https://www.inria.fr/en"><strong> INRIA </strong></a> and <a href="https://pages.saclay.inria.fr/frederic.alauzet/"><strong> Frederic Alauzet </strong></a>.
	                {/* start spacing*/}
        		<div class="columns">
            			<div class="column is-12"></div>
        		</div>
      			{/* end spacing */}
      			<Link to='/my-site/remeshing'>
    				<button class="button is-link">
          				About
    				</button>
			</Link>
			{/* start spacing*/}
						  <div class="columns">
      				<div class="column is-12"></div>
  			</div>
  			<div class="columns">
      				<div class="column is-12"></div>
			</div>
  			{/* end spacing */}
			<img src={require('./images/tatef.png')} class="center"/>
  
	  </div>
{/*	{[0,1,2,3,4,5,6,7,8,9,10].map(i =>
		<div key={i}>
			<h1>{i}</h1>
			Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
			irmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
			voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
			clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
			amet.
		</div>
		)}*/}
	</Timeline>
</StyleRoot>
</div>);
}
export default Home
