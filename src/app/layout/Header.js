import React from 'react'
import { NavLink} from 'react-router-dom'

class Header extends React.Component {

  state = {
    isActive: false,
  }

  toggleNav = () => {
    this.setState(prevState => ({
      isActive: !prevState.isActive
    }))
  }
  render() {
    return (
      <nav className="navbar"
          aria-label="main navigation"
          style={{
            borderBottom: 'solid 1px #dddddd',
          }}>
        <div className="navbar-brand">
<NavLink
  className="navbar-item"
  to="/my-site"
>
  <img
    style={{
      borderTopLeftRadius: '50%',
      borderTopRightRadius: '50%',
      borderBottomLeftRadius: '50%',
      borderBottomRightRadius: '50%',
      marginRight: 15
    }}
    src="https://gitlab.com/uploads/-/system/user/avatar/2067265/avatar.png?width=400"
    width="30px"
    alt=""
  />
  <span>Alberto Remigi</span>
</NavLink>
	    <button className={ this.state.isActive ? 'button navbar-burger is-active' : 'button navbar-burger'} onClick={this.toggleNav}
            aria-label="menu"
           aria-expanded="false"
           data-target="navbarBasicExample"
	   >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
        </div>
        <div id="navbarBasicExample" className={ this.state.isActive ? 'navbar-menu is-active' : 'navbar-menu'}>
          <div className="navbar-start">
	  <NavLink className="navbar-item" to="/my-site/blog" activeClassName="is-active" onClick={this.toggleNav}>
 	   <span className="icon" style={{ marginRight: 5 }}>
    	   <i className="fas fa-code"></i>
  	   </span>
  	    Blog
	    </NavLink>
	    <NavLink className="navbar-item" to="/my-site/trips" onClick={this.toggleNav}>
              <span className="icon" style={{ marginRight: 5 }}>
                <i className="fas fa-lg fa-globe"></i>
              </span>
              Trips
            </NavLink>
            <div className="navbar-item has-dropdown is-hoverable">
              <a className="navbar-link" onClick={this.toggleNav}>
              <span className="icon" style={{ marginRight: 5 }}>
                <i className="fas fa-lg fa-fighter-jet"></i>
              </span>
	         Projects
              </a>
              <div className="navbar-dropdown">
                <NavLink className="navbar-item" to="/my-site/overview" activeClassName="is-active" onClick={this.toggleNav}>
                  Overview
                </NavLink>
                <hr className="navbar-divider" />
                <NavLink className="navbar-item" to="/my-site/site" activeClassName="is-active" onClick={this.toggleNav} >
                  This Site
                </NavLink>
                <NavLink className="navbar-item" to="/my-site/master" activeClassName="is-active" onClick={this.toggleNav} >
                  Master Thesis
                </NavLink>
                <NavLink className="navbar-item" to="/my-site/PhD" activeClassName="is-active" onClick={this.toggleNav} >
                  PhD
                </NavLink>
                <NavLink className="navbar-item" to="/my-site/graph" activeClassName="is-active" onClick={this.toggleNav} >
                  Graph
                </NavLink>
                <NavLink className="navbar-item" to="/my-site/remeshing" activeClassName="is-active" onClick={this.toggleNav} >
                  Remeshing
                </NavLink>
	      </div>
	    </div>
          	    <NavLink className="navbar-item" to="/my-site/ContactMe" onClick={this.toggleNav}>
              <span className="icon" style={{ marginRight: 5 }}>
                <i className="fas fa-lg fa-paper-plane"></i>
              </span>
              Contact Me
            </NavLink>
 
          </div>
          <div className="navbar-end">
            <a className="navbar-item" href="https://github.com/albiremo" onClick={this.toggleNav} >
              <span className="icon" style={{ color: '#000000' }}>
                <i className="fab fa-lg fa-github"></i>
              </span>
            </a>
	    <a className="navbar-item" href="https://gitlab.com/albiremo" onClick={this.toggleNav} >
              <span className="icon">
                <i className="fab fa-lg fa-gitlab"></i>
              </span>
            </a>
            <a className="navbar-item" href="https://twitter.com/albiremo" onClick={this.toggleNav} >
              <span className="icon" style={{ color: '#000000' }}> 
                <i className="fab fa-lg fa-twitter"></i>
              </span>
            </a>
            <a className="navbar-item" href="https://www.linkedin.com/in/albertoremigi/" onClick={this.toggleNav} >
              Resume
              <span className="icon" style={{ color: '#000000', marginLeft: 5 }}>
                <i className="fab fa-lg fa-linkedin"></i>
              </span>
            </a>
            <a className="navbar-item" href="https://www.researchgate.net/profile/Alberto-Remigi" onClick={this.toggleNav} >
            <span className="icon" style={{ color: '#000000'}}>
              <i className='fab fa-lg fa-researchgate'></i>
            </span>
            </a>
            <a className="navbar-item" href="https://www.strava.com/athletes/10592699" onClick={this.toggleNav} >
            <span className="icon" style={{ color: '#000000' }} >
              <i className='fab fa-lg fa-strava'></i>
            </span>
            </a>
            <a className="navbar-item" href="https://stackexchange.com/users/13080973/albiremo?tab=accounts" onClick={this.toggleNav} >
            <span className="icon" style={{ color: '#000000' }} >
              <i className='fab fa-lg fa-stack-exchange'></i>
            </span>
            </a>

	    </div>
        </div>
      </nav>
    )
  }
}

export default Header
