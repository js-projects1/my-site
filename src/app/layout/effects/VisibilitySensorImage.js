import React, { Component} from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import ProgressBar from '../ProgressBar';

class VisibilitySensorImage extends Component {
  state = {
    visibility: false
  }

  render() {
    return (
      <VisibilitySensor
        onChange={(isVisible) => {
          this.setState({visibility: isVisible})
        }}
      >
      <ProgressBar color={"#2f65ed"} width={"150px"} value={this.state.visibility ? this.props.value : 0} max={100} />
      </VisibilitySensor>
    );
  }
}

export default VisibilitySensorImage;
