import styled from 'styled-components'
import React from "react";

const Barra = styled.div`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0.5em 1em;
  padding: 0.25em 1em;

  ${props => props.primary && css`
    background: palevioletred;
    color: white;
  `}
`;



const ProgressBar = (props) => {
  const { value, max } = props;
  return (
      <Barra>
        <progress value={value} max={max} />
	 <span>{(value/max)*100}%</span>
      </Barra>
  );
};

ProgressBar.propTypes = {
	value: PropTypes.number.isRequired,
	max: PropTypes.number
}

ProgressBar.defaultProps = {
	max: 100
}

export default ProgressBar;



