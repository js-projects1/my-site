import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './../Home'
import Blog from './../Blog'
import Trips from './../Trips'
import Overview from './../Overview'
import Site from './../Site'
import PhD from './../PhD'
import Master from './../Master'
import Graph from './../Graph'
import Remeshing from './../Remeshing'
import ContactMe  from './../ContactMe'
const Router = () => (
  <Switch>
    <Route exact path='/my-site' component={Home}/>
    <Route path='/my-site/blog' component={Blog}/>
    <Route path='/my-site/trips' component={Trips}/>
    <Route path='/my-site/overview' component={Overview}/>
    <Route path='/my-site/site' component={Site}/>
    <Route path='/my-site/PhD' component={PhD}/>
    <Route path='/my-site/master' component={Master}/>
    <Route path='/my-site/graph' component={Graph}/>
    <Route path='/my-site/remeshing' component={Remeshing}/>
    <Route path='/my-site/ContactMe' component={ContactMe}/>
  </Switch>
)
export default Router
