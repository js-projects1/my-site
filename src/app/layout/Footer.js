import React from 'react'

const Footer = () => (
  <footer className="footer">
    <div className="container">
      <div className="content has-text-centered">
        <p>
          Built on <a href="https://fr.reactjs.org/"><strong>React</strong></a>
          <span className="icon" style={{ color: '#0077B5', marginLeft: 5 }}>
            <i className='fab fa-lg fa-react'></i>
          </span>
          (<a href="https://developer.mozilla.org/fr/docs/Web/JavaScript"><strong>JavaScript</strong></a>
          <span className="icon" style={{ color: '#0077B5', marginLeft: 5 }}>
            <i className='fab fa-lg fa-js'></i>
          </span>)
          with <a href="https://bulma.io/"><strong>Bulma</strong></a> by Me! <a href="https://gitlab.com/albiremo">Alberto Remigi</a>
          <br/>
          Copyright 2023
        </p>
      </div>
    </div>
  </footer>
)

export default Footer
