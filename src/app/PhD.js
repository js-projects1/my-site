import React from 'react'
const PhD = () => (
  <section class="hero">
        <div class="container is-max-desktop">
    <div class="hero-body has-text-centered">
      <p class="title">
      Numerical modeling of an aeronautical injector: from the internal flow to the dispersed spray
      </p>
      <p class="subtitle">
        Developement of a multiphase model and of an innovative phase analysis
      </p>
      </div>
      The more stringent regulation about aeronautical engines emission posed by ICAO requires always more predictive design tools. The droplets diameter distribution produced during the atomization process is a key parameter in order to predict the pollutant emission released during the combustion process. Thus the study of the atomization phenomenon with its multi-scale nature is a relevant and an important challenge.
      For this reason the objective of this work are: first to review the existent models in the literature to understand their key features in order to define a classification that gives guidelines on the modeling choices; second to apply industrial oriented approaches on an aeronautical configuration, in order to propose an improvement of the available design tools.
      A systematic classification of the models is done with respect to the length-scale considered to represent the interface characteristics.  From this point of view, it is possible to distinguish two kinds of approaches: the separated phases representation and the mixed phases representation. The diffuse interface approaches belongs on the second category together with many other approaches, compressible and incompressible, that share the same characteristic: they considers a mixture that contains both phases.
      An air-assisted liquid sheet configuration has been built to test different models in order to define a metric of comparison. Two different models using the sharp interface approach (ARCHER and interFoam), two models using the diffuse interface approach (CEDRE and ELSA) and an hybrid model (icmElsa) have been considered on this test case. A comparison on two parts, based on statistical quantities, has been proposed. A fist part called "classical study", compare the first order statistics showing that all approaches lead to very similar results, as soon as certain level of mesh resolution is achieved. At the contrary the second  order statics present noticeable differences. These results motivate a  second part called "phase analysis" to study the link between the small scale representation of the interface and the second-order statics. In particular, the phase marker variance  and the associated segregation level are found to be sensible indicators of the interface description. A 1D signal analysis shows that they can be used to detect any departure from the separated phases representation. Then the importance of the phase indicator variance is demonstrated on other second-order statistics: Reynolds stress components and turbulent liquid flux. Thus, second-order statistics are partly described with direct mixed phases representations and require complementary model to be fully recovered. A first attempt, based on a  linear approach, is proposed to model the level of segregation of mixed phases representation. It is based on the filtering of a fully segregated signal at a given scale.
      In a second part of this thesis, an industrial test case (a pressure swirling injector) proposed by SAFRAN Aircraft Engines is studied. Three industrial oriented models,  among those studied in the first part, have been applied to simulate this injector flow (interFoam, ELSA, icmElsa). Their present numerical approaches are able to work with complex geometries, with a computational effort  representative of the industrial current standards. The results of the three models (liquid film thickness, breakup length and Sauter Mean Diameter) have been compared  with respect to the available experimental data. Eventually, a proposal to improve the icmElsa model multi-scale have been successfully tested on the liquid sheet configuration and implemented to further improve the results of the SAFRAN Aircraft Engines industrial case. These results have shown that we are very close to predict the characteristics of a spray produced by a real aeronautical injection system.
</div>
{/* start spacing*/}
  <div class="columns">
      <div class="column is-12">
      </div>
  </div>
{/* end spacing */}
<div class="block has-text-centered">
<button class="button is-rounded">
  <span class="icon is-small">
    <i class="fa fa-download"></i>
  </span>
       <a href="https://tel.archives-ouvertes.fr/tel-03231663">Thesis</a>
</button>
<button class="button is-rounded">
  <span class="icon is-small">
    <i class="fa fa-download"></i>
  </span>
       <a href="https://hal.archives-ouvertes.fr/hal-02350200/document">Article</a>
</button>
</div>
  </section>
)
export default PhD
