import React, { useState } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import L from 'leaflet';
import {
    MapContainer, TileLayer, Marker, Popup
} from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import cities from '../data/Places.js'
const Trips = () => {
let DefaultIcon = L.icon({
    iconUrl: icon,
//    shadowUrl: iconShadow,
    iconSize: [7, 7],
    iconAnchor: [3.5, 3.5]
});
L.Marker.prototype.options.icon = DefaultIcon;
  /* Set responsive for changing window size*/
  const responsive = {
    0: { items: 1 },
    568: { items: 2 },
    1024: { items: 3 },
};
  const items = [
    <img src={require('./images/eiffel.jpg')}/>,
    <img src={require('./images/muro.jpg')}/>,
    <img src={require('./images/burji.jpg')}/>,
    <img src={require('./images/nyc.jpg')}/>,
    <img src={require('./images/buda.jpg')}/>,
    <img src={require('./images/louvre.jpg')}/>,
];
  const center = [48.864716, 2.349];
//  const [zoom, setZoom] = useState(0);
  return(
<div>
         <div class="container is-max-desktop">
      <div class="hero-body has-text-centered">
        <p class="title">
          A moving life
        </p>
        <p class="subtitle">
          I lived in 3 countries and explored plenty of cities in the world. I love to discover new cultures.
        </p>
	  </div>
  </div>
<div>
<MapContainer center={center} zoom={3} style={{
                            height:"700px"
                        }} scrollWheelZoom={true}>
    <TileLayer
      attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
         {cities.map(i=>(<Marker  key={i.id}
          position={[i.coordinates[0],i.coordinates[1]]}/>))}
      </MapContainer>

</div>
 
<div class="card is-half">
    <div class="card-content is-half">
      <p class="title">
      Qualunque Cosa Farai... Amala! Come Amavi la Cabina del 'Paradiso'... Quann'eri Picciriddu..
      </p>
      <p class="subtitle">
        Alfredo
      </p>
    </div>
    <footer class="card-footer">
      <p class="card-footer-item">
        <span>
          View on  <i className="fab  fa-youtube"></i>
<a href="https://www.youtube.com/watch?v=ZpQfbyeR1no&ab_channel=Ettore1852">Youtube</a>
        </span>
      </p>
     </footer>
  </div>

{/* start spacing*/}
  <div class="columns">
      <div class="column is-12">
      </div>
  </div>
  <div class="columns">
      <div class="column is-12">
      </div>
  </div>
{/* end spacing */}
<nav class="level">
  <div class="level-item has-text-centered">
    <div>
      <p class="heading">Cities Visited</p>
      <p class="title">{Object.keys(cities).length}</p>
    </div>
  </div>
</nav>
{/* start spacing*/}
<div class="columns">
    <div class="column is-12">
    </div>
</div>
<div class="columns">
    <div class="column is-12">
    </div>
</div>
{/* end spacing */}
    <div className="App">
    <AliceCarousel
        mouseTracking
        items={items}
        autoPlay autoPlayInterval="3000"
        responsive={responsive}
        infinite={true}
   >
   </AliceCarousel>
   </div>
   </div>
    );
}
export default Trips
