# My personal website

A static website built in [React](https://fr.reactjs.org/) with the help of [Bulma](https://bulma.io/) to present myself!

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Useful commands

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.<br />

### `npm run build`
Builds the app for production to the `build` folder.<br />


It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.<br />

A test deployment is run with [Google Firebase](https://firebase.google.com/) following the procedure here:

- You run `npm run build`
- If not installed you install firebase `npm install -g firebase-tools`
- you login into your account `firebase login`
- you initialize your firebase project by following the guided procedure : `firbase init`. Your page is ready to be Deployed!
- `firbase deploy` the commandline will give you the link to where the website is deploied. To put your website down use the command `firebase hosting:disable`.

## Libraries and third party components
- [npm](https://www.npmjs.com/)
- [Node.js](https://nodejs.org/en/)
- [React](https://fr.reactjs.org/)
- [Bulma](https://bulma.io/)
- [Styled components](https://styled-components.com/)
- [Font Awesome](https://fontawesome.com/)
- [react-leaflet](https://react-leaflet.js.org/)
- [leaflet](https://leafletjs.com/)
- [react-alice-carousel](https://github.com/maxmarinich/react-alice-carousel)
- [EmailJS](https://www.emailjs.com/)
- [SweetAlert](https://sweetalert.js.org/)
 
